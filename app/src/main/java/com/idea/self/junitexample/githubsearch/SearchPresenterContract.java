package com.idea.self.junitexample.githubsearch;

public interface SearchPresenterContract {
    void searchGitHubRepos(String searchQuery);
}
