package com.idea.self.junitexample.base;

import android.content.Context;
import dagger.Provides;

public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }
}
