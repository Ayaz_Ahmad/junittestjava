package com.idea.self.junitexample.base;

import com.idea.self.junitexample.githubsearch.GithubSearchActivity;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {RetrofitModule.class, ContextModule.class})
public interface AppComponent {
    void inject(GithubSearchActivity githubSearchActivity);
}
