package com.idea.self.junitexample.githubsearch;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.idea.self.junitexample.R;
import com.idea.self.junitexample.base.MyApp;
import com.idea.self.junitexample.githubsearch.model.SearchResult;
import com.idea.self.junitexample.githubsearch.reository.GitHubApi;
import com.idea.self.junitexample.githubsearch.reository.GitHubRepository;
import retrofit2.Retrofit;

import javax.inject.Inject;
import java.util.List;
import java.util.Locale;

public class GithubSearchActivity extends AppCompatActivity implements SearchViewContract {

    @Inject
    Retrofit retrofit;
    private SearchResultRvAdapter rvAdapter;
    private TextView tvStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_github_search);
        ((MyApp) getApplication()).getAppComponent().inject(this);

        tvStatus = findViewById(R.id.tv_status);
        final EditText etSearchQuery = findViewById(R.id.et_search_query);

        GitHubRepository repository = new GitHubRepository(retrofit.create(GitHubApi.class));
        final SearchPresenterContract presenter = new SearchPresenter(this, repository);

        etSearchQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    presenter.searchGitHubRepos(etSearchQuery.getText().toString());
                    return true;
                }
                return false;
            }
        });

        RecyclerView rvRepos = findViewById(R.id.rv_repos);
        rvRepos.setHasFixedSize(true);
        rvAdapter = new SearchResultRvAdapter();
        rvRepos.setAdapter(rvAdapter);
    }

    @Override
    public void displaySearchResults(@NonNull List<SearchResult> searchResults, @Nullable Integer totalCount) {
        rvAdapter.updateResults(searchResults);
        tvStatus.setText(String.format(Locale.US, "Number of results: %d", totalCount));
    }

    @Override
    public void displayError() {
        Toast.makeText(this, "some error happened", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayError(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
