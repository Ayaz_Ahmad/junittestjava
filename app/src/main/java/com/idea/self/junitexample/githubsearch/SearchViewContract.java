package com.idea.self.junitexample.githubsearch;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.idea.self.junitexample.githubsearch.model.SearchResult;

import java.util.List;

public interface SearchViewContract {
    void displaySearchResults(@NonNull List<SearchResult> searchResults, @Nullable Integer totalCount);

    void displayError();

    void displayError(String s);
}
